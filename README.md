# robot_project
This code runs on a shoebox sized robot featuring two DC motors + encoders, raspberry pi + motor controller, and a camera. steeringtest.py instructs it to follow blue tape by isolating it in the camera output, finding the position, and adjusting the speed of the motors.

Read more about this project here: https://ethanharp.codeberg.page/project/project-3/